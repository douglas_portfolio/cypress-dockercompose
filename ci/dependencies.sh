#!/usr/bin/bash

# Add python pip and bash
apt install python3-pip

# Install docker-compose via pip
pip3 install --no-cache-dir docker-compose
docker-compose -v