import * as oosCa from './workflows/oos-ca'
import { describeTable, tableIt, entryIt } from 'mocha-table'
import * as dotenv from 'dotenv';

describe('Configure environment...', () => {
  before(() => {
    dotenv.config()
  })
})

describe('Existent Crew Members must be found.', () => {

  beforeEach('Load the page', () => { oosCa.go() })
  describeTable('When searched by first name', () => {

    tableIt('Was "%s" found? (should be %t)', (name: string, result: boolean) => {
      oosCa.searchForName(name)
      console.log(oosCa.firstMemberShouldContainName(name))
    })

    entryIt('lloyd', true)
    entryIt('emma', true)
    entryIt('danielle', true)
    entryIt('julia', true)
  })

  describeTable('When searched by last name', () => {

    tableIt('Was "%s" found? (should be %t)', (name: string, result: boolean) => {
      oosCa.searchForName(name)
      console.log(oosCa.firstMemberShouldContainName(name))
    })

    entryIt('gonzalez', true)
    entryIt('stewart', true)
    entryIt('moore', true)
    entryIt('cunningham', true)

  })

  describeTable('When searched by full name', () => {

    tableIt('Was "%s" found? (should be %t)', (name: string, result: boolean) => {
      oosCa.searchForName(name)
      console.log(oosCa.firstMemberShouldContainName(name))
    })

    entryIt('lloyd gonzalez', true)
    entryIt('emma stewart', true)
    entryIt('danielle moore', true)
    entryIt('julia cunningham', true)
  })

})

describe('Inexistent Crew Members must not be found.', () => {
  it('Test01_ChuckNorris should not be found', () => {
    const name = 'Test01_ChuckNorris'
    oosCa.searchForName(name)
    oosCa.firstMemberShouldNotContainName(name)
  })
})

describe('Clear button should clear and refresh', () => {
  it('Must clear input fields', () => {
    oosCa.clickOnClearButton()
    oosCa.nameFiledShouldBeEmpty()
    oosCa.cityFieldShouldBeEmpty()
  })

  it('Must refresh the page', () => {
    oosCa.clickOnClearButton()
    oosCa.firstMemberShouldContainName('lloyd gonzalez')
  })
})
