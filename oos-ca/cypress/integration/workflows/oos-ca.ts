/**
 * Workflow is a design pattern which implements the code reusability of PageObjects, but avoid those issues (they are hard to maintain, insert new state in the application, try to fit multiple cases in an uniform interface and make tests slow);
 * 
 */
import 'cypress-xpath'
import 'chai'

export const environment = () => {
  if (!Cypress.env('APP_URL') || !Cypress.env('APP_URL').includes('http'))
    return { url: 'http://app:5000' }
  else
    return {
      url: `${Cypress.env('APP_URL')}`
    }
}

export const crewApplicationPage = {
  name: '#name',
  city: '#city',
  submitBtn: '[type = "submit"]',
  clearBtn: '#filters > [type="button"]',
  member: {
    firstMemberLocator: '.CrewMemeber-name > :nth-child(1)',
    crewMemberName: 'div[class="CrewMember-name"]',
  },
}

//Methods

export const go = () => { cy.visit(environment().url) }

export const searchForName = (name: string) => {
  const page = crewApplicationPage
  cy.get(page.name).clear().type(name)
  cy.get(page.submitBtn).click()
}

export const firstMemberShouldContainName = (name: string) => {
  return cy.get(crewApplicationPage.member.firstMemberLocator).should('contain', name)
}

export const firstMemberShouldNotContainName = (name: string) => {
  return cy.get(crewApplicationPage.member.firstMemberLocator).should(($el) => {
    chai.expect($el).to.not.exist
  })
}

export const crewMemberNameContains = (name: string) => {
  return cy.get(crewApplicationPage.member.crewMemberName).contains(name)
}

export const promoteCrewMember = (name: string) => {
  cy.xpath(`(//div[text()="${name}"]//following::button[contains(@class, 'CrewMember-up')])[1]`).click()
}

export const backCrewMember = (name: string) => {
  cy.xpath(`(//div[text()="${name}"]//following::button)[1]`).click()
}

export function clickOnClearButton() {
  cy.get('#filters > [type="button"]').click()
}

const fieldMustBeEmpty = (locator: string) => {
  cy.get(locator).should(($el) => {
    chai.expect($el).to.be.empty
  })
}

export const nameFiledShouldBeEmpty = () => fieldMustBeEmpty(crewApplicationPage.name)
export const cityFieldShouldBeEmpty = () => fieldMustBeEmpty(crewApplicationPage.name)