# CREW APPLICATION

Simple application which represents dashboard with candidates.
Before all, you need to go to the folder `oos-ca`.

### Running the App locally
1. Install all the dependencies with `yarn install`
2. Start the app with`yarn start`

App will be available on http://localhost:3000

### Running the Automation locally
1. Run the app in a console
2. Open a second console and run the following commands:
   a. ` yarn run e2e --env APP_URL=http://localhost:3000`

### Running in docker

Grant you have `docker-compose` installed in your machine.
Just run `docker-compose up`.
